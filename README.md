# NAME

CCP::Collector - domain logic for CCP collection platform

# VERSION

This module is part of distribution CCP-Collector v1.0.0.

This distribution's version numbering follows the conventions defined at [semver.org](http://semver.org/).

# DESCRIPTION

This distribution comprises a Perl API for the domain logic of the CCP
collection platform.

# METHODS

## register\_device

    my $secret = ... ;
    try {
        my $key = $self->register_device($secret);
    }
    catch (CCP::Collector::Error::BadSecret $e) {
        die 'an invalid secret was supplied';
    }

Obtain a unique device ID provided that the correct secret is provided.

The secret is currently hard-coded to `12345678`.

## store

    my $key = $self->register_device( ... );
    my $json = ... ;

    try {
        $self->store($key, $json);
    }
    catch (CCP::Collector::Error::BadKey $e) {
        die 'was this device properly registered?';
    }

Given a previously-obtained device key and a blob of JSON representing
sensor data, store this data into the database.

No meaningful data is returned by this method.

# SUPPORT

## Bugs / Feature Requests

Please report any bugs or feature requests by email to `bug-ccp-collector at rt.cpan.org`, or through
the web interface at [http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CCP-Collector](http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CCP-Collector). You will be automatically notified of any
progress on the request by the system.

## Source Code

The source code for this distribution is available online in a [Git](http://git-scm.com/) repository.  Please feel welcome to contribute patches.

[https://github.com/xxx/xxx](https://github.com/xxx/xxx)

    git clone git://github.com/xxx/xxx

# AUTHOR

Alex Peters <s3105178@student.rmit.edu.au>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2014 by Alex Peters.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

The full text of the license can be found in the
`LICENSE` file included with this distribution.
