package CCP::Collector::Error::BadKey;

=head1 DESCRIPTION

Thrown when a non-existent device key is given to
L<CCP::Collector/store>.

=cut

use Moo 0.9.1; # any version seems to be okay
use Throwable 0.200000; # Moo port

with 'Throwable';

1;
