package CCP::Collector::Error::BadSecret;

=head1 DESCRIPTION

Thrown when a bad secret identifier is passed to
L<CCP::Collector/register_device>.

=cut

use Moo 0.9.1; # any version seems to be okay
use Throwable 0.200000; # Moo port

with 'Throwable';

1;
