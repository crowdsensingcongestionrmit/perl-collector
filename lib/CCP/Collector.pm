package CCP::Collector v1.0.0;
# ABSTRACT: domain logic for CCP collection platform

=head1 DESCRIPTION

This distribution comprises a Perl API for the domain logic of the CCP
collection platform.

=cut

use CCP::Collector::Schema 1 ();
use CCP::Collector::Error::BadKey ();
use CCP::Collector::Error::BadSecret ();

use Moose; # any version probably okay
use Data::Random::String (); # any version okay
use JSON 'decode_json';
use POSIX 'strftime'; # core dependency

has 'schema' => (
    is => 'ro',
    isa => 'CCP::Collector::Schema',
    builder => '_build_schema',
    lazy => 1,
);

sub _build_schema {
    return CCP::Collector::Schema->connect_using_config;
}


=method register_device

    my $secret = ... ;
    try {
        my $key = $self->register_device($secret);
    }
    catch (CCP::Collector::Error::BadSecret $e) {
        die 'an invalid secret was supplied';
    }

Obtain a unique device ID provided that the correct secret is provided.

The secret is currently hard-coded to C<12345678>.

=cut

sub register_device {
    my ($self, $secret) = @_;
    my @now = gmtime;

    CCP::Collector::Error::BadSecret->throw
        if not defined $secret or $secret ne '12345678';

    my $key;
    $self->schema->txn_do(sub {
        while (not $key) {
            $key = Data::Random::String->create_random_string;
            undef $key if $self->schema->devices->find({ key => $key });
        }
        $self->schema->devices->create({
            key      => $key,
            added    => strftime('%Y-%m-%d %H:%M:%S', @now),
        });
    });
    return $key;
}

=method store

    my $key = $self->register_device( ... );
    my $json = ... ;

    try {
        $self->store($key, $json);
    }
    catch (CCP::Collector::Error::BadKey $e) {
        die 'was this device properly registered?';
    }

Given a previously-obtained device key and a blob of JSON representing
sensor data, store this data into the database.

No meaningful data is returned by this method.

=cut

sub store {
    my ($self, $key, $json) = @_;
    my @now = gmtime;

    # Transaction not necessary because keys don't change.
    my $device = $self->schema->devices->find({ key => $key })
        or CCP::Collector::Error::BadKey->throw;

    my $ok = eval {
        my $data = decode_json $json;

        $self->schema->txn_do(sub {
            my $submission = $device->add_to_submissions({ added => strftime('%Y-%m-%d %H:%M:%S', @now) });
            if (exists $data->{'Accelerometer'}) {
                for my $reading (@{ $data->{'Accelerometer'} }) {
                    # Future optimisation: POPULATE, not INSERT.
                    $submission->add_to_accelerometer_readings({
                        time    => $reading->{'timestamp'},
                        x_value => $reading->{'x'} * 10 ** 8,
                        y_value => $reading->{'y'} * 10 ** 8,
                        z_value => $reading->{'z'} * 10 ** 8,
                    });
                }
            }
use Math::Round 'round';
            if (exists $data->{'Barometer'}) {
                for my $reading (@{ $data->{'Barometer'} }) {
                    # Future optimisation: POPULATE, not INSERT.
                    $submission->add_to_barometer_readings({
                        time        => $reading->{'timestamp'},
                        pressure    => round($reading->{'pressure'} * 10 ** 2),
                    });
                }
            }
            if (exists $data->{'Magnetometer'}) {
                for my $reading (@{ $data->{'Magnetometer'} }) {
                    # Future optimisation: POPULATE, not INSERT.
                    $submission->add_to_magnetometer_readings({
                        time    => $reading->{'timestamp'},
                        x_value => round($reading->{'x'} * 10 ** 2),
                        y_value => round($reading->{'y'} * 10 ** 2),
                        z_value => round($reading->{'z'} * 10 ** 2),
                    });
                }
            }
            if (exists $data->{'GPSReadings'}) {
                for my $reading (@{ $data->{'GPSReadings'} }) {
                    # Future optimisation: POPULATE, not INSERT.
                    $submission->add_to_gps_readings({
                        time        => $reading->{'ScanTime'},
                        latitude    => $reading->{'Latitude'} * 10 ** 7,
                        longitude   => $reading->{'Longitude'} * 10 ** 7,
                        accuracy    => round($reading->{'Accuracy'} * 10 ** 3),
                    });
                }
            }
            if (exists $data->{'WifiScans'}) {
                for my $scan_data (@{ $data->{'WifiScans'} }) {
                    my $scan = $submission->add_to_wifi_scans({
                        start_time  => $scan_data->{'ScanStartTime'},
                        end_time    => $scan_data->{'ScanEndTime'},
                    });
                    for my $reading (@{ $scan_data->{'WifiReading'} }) {
                        my $mac = $reading->{'MacAddress'};
                        $mac =~ s/://g;
                        $scan->add_to_readings({
                            time            => $reading->{'ScanTime'},
                            mac_address     => $mac,
                            network_name    => $reading->{'NetworkName'},
                            signal_level    => $reading->{'Signal'},
                        });
                    }
                }
            }
        });

        1;
    };
    if (not $ok) {
        my $error = $@;
        die "oh no $error";
    }

    # Don't inadvertently return a database row!
    return;
}

1;
